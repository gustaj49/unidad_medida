<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConversorController extends Controller
{
	private $longitud;

	public function __construct() {
		$this->longitud = collect([
			['id' => 1, 'name' => 'Metro (m)', 'pos' => 0],
			['id' => 2, 'name' => 'Decímetro (dm)', 'pos' => -1],
			['id' => 3, 'name' => 'Centímetro (cm)', 'pos' => -2],
			['id' => 4, 'name' => 'Kilómetro (km)', 'pos' => 2],
		]);
	}

	public function index() {
		return view('conversor.index');
	}

	public function getValueConverted(Request $request) {
		$some       = (float) $request->input('some');
		$unit       = $request->input('unit');
		$first_unit = $request->input('first_unit');
		$new_value  = $request->input('new_value');
		
		$unit_pos       = $this->longitud->where('id', $unit)->first()['pos'];
		$first_unit_pos = $this->longitud->where('id', $first_unit)->first()['pos'];

		$diff   = ($first_unit_pos) - ($unit_pos);
		// $diff   = abs($diff);
		$result = 0;

		// var_dump(round(0.01 * (10 ** 2), 7));

		if ($diff > 0) {
			$result = round($some * (10 ** abs($diff)), 7);
		} else {
			$result = round($some / (10 ** abs($diff)), 7);
		}

		return response()->json(['result' => $result]);

	}

	public function getUnits(Request $request) {
		/**
		 * TODO
		 * Consultar BD para tipo de unidades de medidas
		 */
		$id = $request->input('type');

		return response()->json($this->longitud);
	}
}
