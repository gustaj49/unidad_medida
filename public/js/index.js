var vm = new Vue({
	el: '#conversor',
	data: function () {
		return {
			unit       : 0,
			some       : 0,
			first_unit : 0,
			changed    : false,
			list_units : []
		}
	},
	computed: {
	},
	beforeCreate: function () {
		this.type = jQuery('#conversor').data('type');
	},
	created: function () {
		var that = this;

		axios.post('/units', {
			unit: this.type
		})
		.then(function (response) {
			var units = response.data;

			that.list_units = units;
			// that.list_units = Object.assign({}, that.list_units, units);
		});
	},
	watch: {
		some: function (val) {
			if (val > 0) {
				if ((!this.changed)) {
					this.changed = true;
					this.first_unit = this.unit;
				}
			}
		},
		unit: function (val) {
			var that = this;
			console.log(this.some);
			if (this.changed) {
				axios.post('/', {
					some       : this.some,
					unit       : this.unit,
					first_unit : this.first_unit
				})
				.then(function (response) {
					var result = response.data.result;
					that.some = parseFloat(result);
					that.first_unit = that.unit;
				});
			}
		},
	}
});
