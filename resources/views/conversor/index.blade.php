<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Prueba conversor</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
</head>
<body>
	{{ csrf_field() }}

	<div class="col text-center">
		<h1>Prueba conversor</h1>
		<hr>
	</div>
	<div id="conversor" class="container" data-type="longitud">
		<div class="row">
			<div class="col">
				<input class="form-control" type="number" name="some" v-model="some" v-bind:disabled="unit == 0">
			</div>

			<div class="col">
				<select id="" class="form-control" name="units" v-model="unit">
					<option value="0" selected="selected">Seleccione una unidad de medida</option>
					<option v-for="(item, index) in list_units" v-bind:value="item.id">@{{ item.name }}</option>
				</select>
			</div>
		</div>

	</div>

	<script src="{{ asset('js/jquery.slim.min.js') }}"></script>
	<script src="{{ asset('js/popper.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.js') }}"></script>
	<script src="{{ asset('js/axios.min.js') }}"></script>
	<script src="{{ asset('js/vue.js') }}"></script>
	<script src="{{ asset('js/index.js') }}"></script>
</body>
</html>
